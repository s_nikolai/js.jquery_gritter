js.jquery_gritter
=================

.. contents::

Introduction
------------

This library packages `Gritter for jQuery`_ for `fanstatic`_.

.. _`fanstatic`: http://fanstatic.org
.. _`Gritter for jQuery`: https://github.com/jboesch/Gritter

This requires integration between your web framework and ``fanstatic``,
and making sure that the original resources (shipped in the ``resources``
directory in ``js.jquery_gritter``) are published to some URL.